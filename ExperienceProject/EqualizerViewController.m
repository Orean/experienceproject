//
//  EqualizerViewController.m
//  ExperienceProject
//
//  Created by Galean Pallerman on 9/25/17.
//  Copyright © 2017 Gallean Pallerman. All rights reserved.
//

#import "EqualizerViewController.h"

@implementation EqualizerViewController{
    UISwipeGestureRecognizer *_swipeRecognizer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDetected:)];
    [_swipeRecognizer setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:_swipeRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)swipeDetected:(UISwipeGestureRecognizer *)recognizer{
    [self dissmissSelf];
}

- (void)dissmissSelf{
    CATransition *customTransitionDismiss = [CATransition animation];
    customTransitionDismiss.duration = 0.3;
    customTransitionDismiss.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    customTransitionDismiss.type = kCATransitionPush;
    customTransitionDismiss.subtype = kCATransitionFromBottom;
    [self.view.window.layer addAnimation:customTransitionDismiss forKey:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}


//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    NSLog(@"dest - %@", [segue destinationViewController]);
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}


@end
