//
//  ViewController.h
//  ExperienceProject
//
//  Created by Galean Pallerman on 9/25/17.
//  Copyright © 2017 Gallean Pallerman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AudioUnit/AudioUnit.h>

#import "MainView.h"

@interface MainViewController : UIViewController
<AVAudioRecorderDelegate, AVAudioPlayerDelegate>

@property (nonatomic, weak) MainView *mainView;

@end

