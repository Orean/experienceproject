//
//  ViewController.m
//  ExperienceProject
//
//  Created by Galean Pallerman on 9/25/17.
//  Copyright © 2017 Gallean Pallerman. All rights reserved.
//

#import "MainViewController.h"
#import "EqualizerViewController.h"

@implementation MainViewController{
    UISwipeGestureRecognizer *_swipeRecognizer;
    AVAudioSession *_audioSession;
    AVAudioRecorder *_audioRecorder;
    AVAudioPlayer *_audioPlayer;
    AudioMode _currentMode;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _currentMode = AudioModeRecord;
    
    [self initializeAudioSession];
    
    [self initializeGestures];
    
    self.mainView = (MainView*)self.view;
}

//===================================================
#pragma mark Initializing methods
//===================================================

- (void)initializeAudioSession{
    
    NSError *error;
    
    // Audio session setup
    _audioSession = [AVAudioSession sharedInstance];
    AVAudioSessionCategoryOptions options = AVAudioSessionCategoryOptionAllowBluetoothA2DP | AVAudioSessionCategoryOptionAllowBluetooth | AVAudioSessionCategoryOptionAllowAirPlay;
    [_audioSession setCategory:AVAudioSessionCategoryPlayAndRecord
                                            mode:AVAudioSessionModeDefault
                                         options:options error:&error];
    [self checkError:error];
    [_audioSession overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
    [self checkError:error];
    
    // Setup tmp recording path
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudioMemo.m4a",
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    _audioRecorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    _audioRecorder.delegate = self;
    _audioRecorder.meteringEnabled = YES;
    [_audioRecorder prepareToRecord];
}

- (void)checkAllPrivacyAccepted{
    
}

- (void)initializeGestures{
    //Swipe up to show equalizer
    _swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDetected:)];
    [_swipeRecognizer setDirection:UISwipeGestureRecognizerDirectionUp];
    [self.view addGestureRecognizer:_swipeRecognizer];
}

//===================================================
#pragma mark UI Methods
//===================================================

- (IBAction)startButtonClicked:(id)sender{
    [self playPause];
}

- (IBAction)stopButtonClicked:(id)sender{
    [self stop];
}

- (IBAction)audioModeChanged:(id)sender{
    UISegmentedControl *audioModeControl = (UISegmentedControl *)sender;
    if ( ![audioModeControl.class isSubclassOfClass:[UISegmentedControl class]] )
        assert(nil);
    
    //stop current audio mode
    [self stop];
    
    //change mode
    _currentMode = audioModeControl.selectedSegmentIndex;
}

- (void)playPause{
    switch (_currentMode) {
        case AudioModeRecord:
            [self recordAction];
            break;
            
        case AudioModePlay:
            [self playAction];
            break;
            
        case AudioModeSynchronous:
            break;
            
        default:
            break;
    }
}

- (void)stop{
    switch (_currentMode) {
        case AudioModeRecord:
            [self stopRecording];
            break;
            
        case AudioModePlay:
            [self stopPlaying];
            break;
            
        case AudioModeSynchronous:
            break;
            
        default:
            break;
    }
}

- (void)recordAction{
    if (!_audioRecorder.recording) {
        [self startRecording];
    } else {
        [self pauseRecording];
    }
}

- (void)playAction {
    if ( !_audioPlayer.playing ){
        [self startPlaying];
    }else{
        [self pausePlaying];
    }
}

//===================================================
#pragma mark Start record/play handling
//===================================================

- (void)startRecording{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:nil];
    
    [self resumeRecording];
}

- (void)resumeRecording{
    [_audioRecorder record];
    
    [self.mainView changeStartButton:YES];
}

- (void)startPlaying{
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:_audioRecorder.url error:nil];
    [_audioPlayer setDelegate:self];
    
    [self resumePlaying];
}

- (void)resumePlaying{
    [_audioPlayer play];

    [self.mainView changeStartButton:YES];
}

//===================================================
#pragma mark Stop record/play handling
//===================================================

- (void)pauseRecording{
    [_audioRecorder pause];
    
    [self.mainView changeStartButton:NO];
}

- (void)stopRecording{
    [_audioRecorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}

- (void)pausePlaying{
    [_audioPlayer pause];
    
    [self.mainView changeStartButton:NO];
}

- (void)stopPlaying{
    [_audioPlayer stop];
}

//===================================================
#pragma mark Audio Player Delegate
//===================================================

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [self.mainView changeStartButton:NO];
}

-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error{
    [self checkError:error];
}

//===================================================
#pragma mark Audio Recorder Delegate
//===================================================

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    [self.mainView changeStartButton:NO];
}

-(void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error{
    [self checkError:error];
}

//===================================================
#pragma mark Error Handling
//===================================================

- (void)checkError:(NSError *)error{
    if ( error ){
        NSLog(@"____ERROR: %@", error.description);
    }
}

//===================================================
#pragma mark Gesture Recognizers' Actions
//===================================================

//show equalizer view with up direction swipe
- (void)swipeDetected:(UISwipeGestureRecognizer *)recognizer{
    [self presentEqualizerController];
}

//===================================================
#pragma mark Segue actions
//===================================================

- (void)presentEqualizerController{
    CATransition *customTransition = [CATransition animation];
    customTransition.duration = 0.3;
    customTransition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    customTransition.type = kCATransitionPush;
    customTransition.subtype = kCATransitionFromTop;
    [self.view.window.layer addAnimation:customTransition forKey:nil];
    [self performSegueWithIdentifier:@"ShowEqualizer" sender:self];
}

////===================================================
//#pragma mark - Navigation
////===================================================
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}


//===================================================
#pragma mark TO DO
//===================================================
#pragma mark AudioSession Interuptions Handling
#pragma mark Synchronous playthrough
#pragma mark Equalizer
#pragma mark Themes

@end
