//
//  MainView.m
//  ExperienceProject
//
//  Created by Galean Pallerman on 9/25/17.
//  Copyright © 2017 Gallean Pallerman. All rights reserved.
//

#import "MainView.h"

@implementation MainView

- (void)changeStartButton:(BOOL)isPlaying{
    [_startButton setSelected:isPlaying];
}

@end
