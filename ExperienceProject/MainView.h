//
//  MainView.h
//  ExperienceProject
//
//  Created by Galean Pallerman on 9/25/17.
//  Copyright © 2017 Gallean Pallerman. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    AudioModeRecord,
    AudioModePlay,
    AudioModeSynchronous
} AudioMode;

@interface MainView : UIView{
    IBOutlet UIButton *_startButton;
    IBOutlet UIButton *_stopButton;
    IBOutlet UISegmentedControl *_modesControl;
}

- (void)audioModeChanged:(AudioMode)currentMode;
- (void)changeStartButton:(BOOL)isPlaying;

@end
