//
//  EqualizerSlider.h
//  ExperienceProject
//
//  Created by Galean Pallerman on 9/25/17.
//  Copyright © 2017 Gallean Pallerman. All rights reserved.
//

#import "VerticalSlider.h"

@interface EqualizerSlider : VerticalSlider

@end
