//
//  EqualizerSlider.m
//  ExperienceProject
//
//  Created by Galean Pallerman on 9/25/17.
//  Copyright © 2017 Gallean Pallerman. All rights reserved.
//

#import "EqualizerSlider.h"

@implementation EqualizerSlider

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self) {
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self adjustEqualizerSlider];
    }
    return self;
}

- (void)adjustEqualizerSlider{
    [self setMinimumValue:0];
    [self setMaximumValue:100];
}

@end
